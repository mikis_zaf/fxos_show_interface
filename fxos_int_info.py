# -*- coding: utf-8 -*-
import re
"""
ver 1.1
Returns various outputs related to FXOS interfaces.
"""

# the sw_techsupportinfo is a file with command outputs
with open('sw_techsupportinfo') as mikis_file:
    file_string = mikis_file.read()

# grab the commands
dict_keys = re.findall(r'(?<=`).*(?=`)', file_string, re.MULTILINE)

# grab the command outputs
dict_values = re.findall(r'(?<=`\n).*?(?=`)', file_string, re.MULTILINE | re.DOTALL)

# create a dictionary of command outputs
sw = dict(zip(dict_keys, dict_values))

#try:
# The sw[x].splitlines() returns a list where each line of the command output is a list element
# first I create a list called 'interface_list' of all command outputs that I need (list concatenation)
list_of_commands = ["show port-channel summary", "show lacp counters", "show interface brief"]
# First, I am grabbing the 'show port-channel summary' output from sw_techsupportinfo
show_port_channel_summary_list = sw["show port-channel summary"].splitlines()
		
# Second, I am grabbing from squerewheels the 'show lacp counters' command, putting into a list variable and insert at the beginning 2 new elements
show_lacp_counters_list = sw["show lacp counters"].splitlines()
show_lacp_counters_list.insert(0, "FXOS# show lacp counters")
show_lacp_counters_list.insert(0, " ")
  	
# Third, I am grabbing from squerewheels the 'show interface brief' command, putting into a list variable and insert at the beginning a new element
show_interface_brief_list = sw["show interface brief"].splitlines()
show_interface_brief_list.insert(0, "FXOS# show interface brief")

# List concatenation
interface_list = show_port_channel_summary_list + show_lacp_counters_list + show_interface_brief_list

# Convert the list to one string 'interfaces_string' and add an empty line after each list element
interfaces_string = ""
for j in interface_list:
    interfaces_string += j + '\n'


# return the commands outputs to BORG gateway
res = interfaces_string

# show the result
print(res)
#except:
#    print("Something is wrong with the execution")